console.log('Hello world from openai_search.js!')
// Get the value of the use_javascript from drupalSettings
console.log(drupalSettings.openai_search.use_javascript);
var use_javascript = drupalSettings.openai_search.use_javascript;

function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

waitForElm('#js-submit').then((elm) => {
  console.log('Element is ready');
  console.log(drupalSettings.openai_search.myVariable);
  document.getElementById('js-submit').addEventListener('click', function(e) {
    // Prevent the default form submission
    if (use_javascript === 1) {
      e.preventDefault();
    }
    else {
      return;
    }
    // change the id of spinner to be visible
    document.getElementById('spinner').style.display = 'block';
    // Get the search query
    var searchQuery = document.getElementById('edit-user-search').value;
    console.log('Search query: ' + searchQuery);
    // Send a request to the server with the search query url friendly as an argument to http://main-bvxea6i-is5ueuku5nw4g.us.platformsh.site/json
    // make searchQuery url friendly
    searchQuery = searchQuery.replace(' ', '%20');
    var url = 'https://main-bvxea6i-is5ueuku5nw4g.us.platformsh.site/json/' + searchQuery;
    // Get the response from the server
    var response = fetch(url, { mode: 'cors', headers: {
        'X-Secure-Key': drupalSettings.openai_search.xsecurekey
      }})
      .then(response => {
        if (response.type === 'opaque') {
          // Can't read data from opaque responses
          console.log('Received opaque response. Cannot read data.');
          return;
        }
        return response.json();
      })
      .then(data => {
        if (data) {
          console.log(data);
          document.getElementById('spinner').style.display = 'none';
          // Display the response
          // format any urls in the data to be clickable and open in a new window
          var regex = /(https?:\/\/[^\s]+)/g;
          data = data.replace(regex, "<a href='$1' target='_blank'>$1</a>");
          document.getElementById('response').innerHTML = data;
        }
      });
      console.log(response);

  });
});
