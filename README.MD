<h2>OPENAI SEARCH</h2>
This module allows you to have a private chatgpt instance, and to have it searchable within Drupal. You basically can "GPT" chat with your own Drupal site, and you can add as many documents (pdfs etc) to the knowledgebase as well. Your website can be crawled such that all your pages will be embedded into the private ChatGPT module.

As an added feature, you can give us any documents you wish to be crawled to add to the dataset. Email info@webtechkitchen.com to get an api key for us to setup a GPT instance for you to connect this module to. It works very similar to the Drupal Solr project. We host the GPT server for you to restfully connect to with all your private data, which will be secure, and kept entirely private, and you can expose that data as you like through your Drupal sites (or any type of site actually -- it will work with .net, python, ruby, java, etc)

For those using Drupal a block is available to place anywhere where you can search your data. As well the restful API is available if you want to develop your own use cases against your "GPT" restful endpoint.

<h2>Features</h2>
Rather than simple Drupal site search or Solr search this will allow you to search all of your content via a private OpenAI ChatGPT instance.

<h2>Post-Installation</h2>
Enable this module the usual way. Then reach out to info@webtechkitchen.com to get your api key, and your private ChatGPT restful url.

<h2>Community Documentation</h2>
TODO: Video demo coming.
