<?php

namespace Drupal\openai_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class OpenAIConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openai_search.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_search_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openai_search.settings');

    $form['restful_url'] = [
      '#type' => 'url',
      '#title' => $this->t('RESTful Service URL'),
      '#description' => $this->t('Enter the URL of the RESTful service.'),
      '#default_value' => $config->get('restful_url'),
    ];
    $form['X-Secure-Key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your X-Secure-Key'),
      '#description' => $this->t('Enter the X-Secure-Key you received after signing up for your subscription.'),
      '#default_value' => $config->get('X-Secure-Key'),
    ];
    // add a boolean asking if youd like to use javascript or php to submit the form.
    $form['use_javascript'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use JavaScript to submit the form?'),
      '#default_value' => $config->get('use_javascript'),
    );


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('openai_search.settings')
      ->set('restful_url', $form_state->getValue('restful_url'))
      ->save();
    $this->configFactory->getEditable('openai_search.settings')
      ->set('X-Secure-Key', $form_state->getValue('X-Secure-Key'))
      ->save();
    $this->configFactory->getEditable('openai_search.settings')
      ->set('use_javascript', $form_state->getValue('use_javascript'))
      ->save();

    parent::submitForm($form, $form_state);
  }

 }
