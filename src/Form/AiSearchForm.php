<?php

/**
 * @file
 * Contains \Drupal\openai_search\Form\AiSearchForm.
 */

namespace Drupal\openai_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class AiSearchForm extends FormBase {

  /**
   *  {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_search_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the configuration object.
    $config = \Drupal::config('openai_search.settings');
    // Retrieve the X-Secure-Key value from the configuration.
    $X_Secure_Key = $config->get('X-Secure-Key');
    $form['#attached']['drupalSettings']['openai_search'] = [
      'xsecurekey' => $X_Secure_Key,
      'use_javascript' => $config->get('use_javascript'),
    ];
    $form['user_search'] = array(
      '#type' => 'textfield',
      '#title' => '',
      '#size' => 40,
      '#maxlength' => 60,
    );
    $form['loading-spinner'] = [
      '#markup' => '<div id="css-spinner"><div id="spinner" class="css-spinner" style="display: none;"></div></div>',
    ];
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
      '#attributes' => [
        'id' => 'js-submit', // Adding an ID for easier JavaScript targeting.
        ]
    );
    $form['response'] = [
      '#markup' => '<div id="response"></div>',
    ];
    // Display the response if it exists in the form state.
    if ($form_state->get('response')) {
      $form['response'] = [
        '#markup' => $form_state->get('response'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the form values.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do something useful.
    // Get the value of the search query.
    $user_search = $form_state->getValue('user_search');
    // Make $user_search url friendly.
    $user_search = urlencode($user_search);

    // Get the configuration object.
    $config = \Drupal::config('openai_search.settings');
    // Retrieve the 'restful_url' value from the configuration.
    $restful_url = $config->get('restful_url') . "/" . $user_search;
    $X_Secure_Key = $config->get('X-Secure-Key');

    // Use curl to send the search query to the RESTful service at $restful_url and get the results.
    // Initialize a cURL session.
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $restful_url); // The URL to fetch.
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Return the transfer as a string.
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-Secure-Key: ' . $X_Secure_Key, 'Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false); // Don't include the header in the output.
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); // Follow redirects.

// Execute the cURL session.
    $response = curl_exec($curl);

// Close the cURL session.
    curl_close($curl);

    // Store the response in the form state.
    $form_state->set('response', $response);

    // Rebuild the form.
    $form_state->setRebuild();

  }

}
