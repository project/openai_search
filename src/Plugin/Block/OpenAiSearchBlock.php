<?php

/**
 * @file
 * Contains \Drupal\openai_search\Plugin\Block\OpenAiSearchBlock.
 */

namespace Drupal\openai_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block\Annotation\Block;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a simple block.
 *
 * @Block(
 *   id = "openai_search_block",
 *   admin_label = @Translation("OpenAI Search Module Block"),
 *   module = "openai_search"
 * )
 */
class OpenAiSearchBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    $config = $this->getConfiguration();

    // Here, we assume you're storing the URL in a 'restful_url' config setting.
    // This could be fetched from your module's configuration like so:
    $restful_url = \Drupal::config('openai_search.settings')->get('restful_url');
    $form = \Drupal::formBuilder()->getForm('Drupal\openai_search\Form\AiSearchForm');
    $form['#attached']['library'][] = 'openai_search/openai_search';

    return $form;
  }
}
